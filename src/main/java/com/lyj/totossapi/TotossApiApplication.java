package com.lyj.totossapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TotossApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TotossApiApplication.class, args);
	}

}
